# Fashion Identification from MNIST Dataset

# Much of this code taken from example by:
#@title MIT License
#
# Copyright (c) 2017 François Chollet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.


from __future__ import absolute_import, division, print_function, unicode_literals

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

import tensorflow
from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plot

print(tensorflow.__version__)



######################
## Data Visualization

# Visually plot image.
def plot_image(image):
	plot.figure()
	plot.imshow(image)
	plot.colorbar()
	plot.grid(False)
	plot.show()



######################
## Data Setup

# Load remote data to test network.
fashion_mnist = keras.datasets.fashion_mnist
(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()

# Manual loading of local data?
#from tensorflow.examples.tutorials.mnist import input_data
#data = input_data.read_data_sets('data/fashion')
#data.train.next_batch(BATCH_SIZE)

# Tie class names to indexes.
class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
               'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

print("")
print("Loaded training dataset; shape: " + str(train_images.shape))
print("Loaded training labels; count: " + str(len(train_labels)))
print("Loaded test dataset; shape: " + str(test_images.shape))
print("Loaded test labels; count: " + str(len(test_labels)))

#plot_image(train_images[0])

# Normalize from 0:255 to 0.0:1.0
train_images = train_images / 255.0
test_images = test_images / 255.0

#plot_image(train_images[0])

#plot.figure(figsize=(10,10))
#for i in range(25):
#    plot.subplot(5,5,i+1)
#    plot.xticks([])
#    plot.yticks([])
#    plot.grid(False)
#    plot.imshow(train_images[i])#, cmap=plot.cm.binary)
#    plot.colorbar()
#    plot.xlabel(class_names[train_labels[i]])
#plot.show()



###################
## Model Setup, Fit

model = keras.Sequential([
    keras.layers.Flatten(input_shape=(28, 28)),
    keras.layers.Dense(128, activation='relu'),
    keras.layers.Dense(10, activation='softmax')
])

model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

model.fit(train_images, train_labels, epochs=10)



#######################
## Evaluate and Analyze

test_loss, test_acc = model.evaluate(test_images, test_labels)
predictions = model.predict(test_images)

print('\nTest accuracy:', test_acc)

def plot_image(i, predictions_array, true_label, img):
  predictions_array, true_label, img = predictions_array, true_label[i], img[i]
  plot.grid(False)
  plot.xticks([])
  plot.yticks([])

  plot.imshow(img, cmap=plot.cm.binary)

  predicted_label = np.argmax(predictions_array)
  if predicted_label == true_label:
    color = 'blue'
  else:
    color = 'red'

  plot.xlabel("{} {:2.0f}% ({})".format(class_names[predicted_label],
                                100*np.max(predictions_array),
                                class_names[true_label]),
                                color=color)

def plot_value_array(i, predictions_array, true_label):
  predictions_array, true_label = predictions_array, true_label[i]
  plot.grid(False)
  plot.xticks(range(10))
  plot.yticks([])
  thisplot = plot.bar(range(10), predictions_array, color="#777777")
  plot.ylim([0, 1])
  predicted_label = np.argmax(predictions_array)

  thisplot[predicted_label].set_color('red')
  thisplot[true_label].set_color('blue')



# Plot the first X test images, their predicted labels, and the true labels.
# Color correct predictions in blue and incorrect predictions in red.
num_rows = 5
num_cols = 3
num_images = num_rows*num_cols
plot.figure(figsize=(2*2*num_cols, 2*num_rows))
for i in range(num_images):
  plot.subplot(num_rows, 2*num_cols, 2*i+1)
  plot_image(i, predictions[i], test_labels, test_images)
  plot.subplot(num_rows, 2*num_cols, 2*i+2)
  plot_value_array(i, predictions[i], test_labels)
plot.tight_layout()
plot.show()




#########################################
## Ex: Make prediction about single image

# Grab an image from the test dataset.
#img = test_images[1]

#predictions_single = model.predict(img)

#plot_value_array(1, predictions_single[0], test_labels)
#_ = plt.xticks(range(10), class_names, rotation=45)

