# tensor-flow 
Experiments in ML with Keras + Tensorflow performing basic fashion items identification. 

Dataset and Descriptions from: 
https://github.com/zalandoresearch/fashion-mnist (The MIT License (MIT) Copyright © 2017 Zalando SE, https://tech.zalando.com) 

